package organization.com.mapsample.di.modules;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;
import organization.com.mapsample.di.components.MapsActivityComponent;
import organization.com.mapsample.ui.MapsActivity;

/**
 * Created by dps on 9/27/17.
 */
@Module(includes = MapsActivityModule.Binder.class, subcomponents = MapsActivityComponent.class)
public class MapsActivityModule {
    @Module
    public static abstract class Binder {
        @Binds
        @IntoMap
        @ActivityKey(MapsActivity.class)
        abstract AndroidInjector.Factory<? extends Activity>
        bindYourActivityInjectorFactory(MapsActivityComponent.Builder builder);
    }
}
