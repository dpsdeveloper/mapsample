package organization.com.mapsample.network;

import java.util.List;

import io.reactivex.Observable;
import organization.com.mapsample.models.dto.MarkerDTO;
import retrofit2.http.GET;

/**
 * Retrofit interface description.
 * <p>
 * Created by dps on 9/27/17.
 */
public interface MarkersAPI {
    @GET("api/json/get/bPvorlFkwO?indent=2")
    Observable<List<MarkerDTO>> getMarkers();

}
