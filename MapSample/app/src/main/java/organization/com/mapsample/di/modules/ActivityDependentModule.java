package organization.com.mapsample.di.modules;

import dagger.Module;
import dagger.Provides;
import organization.com.mapsample.interfaces.MapPresenter;
import organization.com.mapsample.interfaces.MapView;
import organization.com.mapsample.network.MarkersAPI;
import organization.com.mapsample.presenter.MapPresenterImpl;
import organization.com.mapsample.ui.MapsActivity;

/**
 * Created by dps on 9/27/17.
 */
@Module
public class ActivityDependentModule {

    private MapsActivity activity;

    public ActivityDependentModule(MapsActivity activity) {

        this.activity = activity;
    }

    @Provides
    MapView provideMapView() {
        return activity;
    }

    @Provides
    MapPresenter providePresenter(MarkersAPI api, MapView view) {
        return new MapPresenterImpl(api, view);
    }
}
