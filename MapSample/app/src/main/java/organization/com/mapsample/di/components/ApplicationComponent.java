package organization.com.mapsample.di.components;

import javax.inject.Singleton;

import dagger.Component;
import organization.com.mapsample.MapSampleApplication;
import organization.com.mapsample.di.modules.ApplicationModule;
import organization.com.mapsample.di.modules.MapsActivityModule;

/**
 * Created by dps on 9/27/17.
 */
@Singleton
@Component(modules = {ApplicationModule.class, MapsActivityModule.class})
public interface ApplicationComponent {

    void inject(MapSampleApplication application);

}
