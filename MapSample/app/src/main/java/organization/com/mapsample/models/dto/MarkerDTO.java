package organization.com.mapsample.models.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Class DTO for get data from Server.
 */
public class MarkerDTO {

    @SerializedName("lng")
    public double lng;

    @SerializedName("description")
    public String description = "";

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title = "";

    @SerializedName("lat")
    public double lat;

    @Override
    public String toString() {
        return
                "MarkerDTO{" +
                        "lng = '" + lng + '\'' +
                        ",description = '" + description + '\'' +
                        ",id = '" + id + '\'' +
                        ",title = '" + title + '\'' +
                        ",lat = '" + lat + '\'' +
                        "}";
    }
}