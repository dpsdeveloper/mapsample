package organization.com.mapsample.models.bo;

/**
 * Class represents model object.
 * By its nature that object is immutable.
 * <p>
 * Created by dps on 9/27/17.
 */
public class MarkerModel {
    public final double lat;
    public final double lng;
    public final String description;

    public MarkerModel(double lat, double lng, String description) {
        this.lat = lat;
        this.lng = lng;
        this.description = description;
    }
}
