package organization.com.mapsample;

import android.app.Activity;
import android.app.Application;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import organization.com.mapsample.di.components.DaggerApplicationComponent;
import organization.com.mapsample.di.modules.ApplicationModule;

/**
 * Extending application for be able to inject activities classes.
 * <p>
 * Created by dps on 9/27/17.
 */
public class MapSampleApplication extends Application implements HasActivityInjector {
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerApplicationComponent.Builder builder = DaggerApplicationComponent.builder();
        builder.applicationModule(new ApplicationModule(this)).build().inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }
}
