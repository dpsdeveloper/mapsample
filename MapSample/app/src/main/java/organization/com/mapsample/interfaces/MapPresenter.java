package organization.com.mapsample.interfaces;

import io.reactivex.Observable;
import organization.com.mapsample.models.bo.MarkerModel;

/**
 * Interface presenter for encapsulate presentation layer.
 * <p>
 * Created by dps on 9/27/17.
 */
public interface MapPresenter {
    /**
     * Will notify presenter about user intents.
     *
     * @param marker - Object.
     */
    void userPressedOnMarker(MarkerModel marker);

    /**
     * Should be called by the clients for get new data from server.
     */
    Observable<MarkerModel> getData();

}
