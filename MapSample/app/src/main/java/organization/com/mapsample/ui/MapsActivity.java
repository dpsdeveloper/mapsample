package organization.com.mapsample.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import organization.com.mapsample.R;
import organization.com.mapsample.interfaces.MapPresenter;
import organization.com.mapsample.interfaces.MapView;
import organization.com.mapsample.models.bo.MarkerModel;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, MapView, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    @Inject
    MapPresenter presenter;

    private GoogleMap mMap;

    private TextView textView;

    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        textView = (TextView) findViewById(R.id.textView);

        compositeDisposable = new CompositeDisposable();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);
        Disposable subscribtion = presenter.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::modelLoaded, throwable -> Log.d("", throwable.getMessage()));
        compositeDisposable.add(subscribtion);
    }

    private void modelLoaded(MarkerModel marker) {
        // Add a marker in Sydney and move the camera
        LatLng latLng = new LatLng(marker.lat, marker.lng);
        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        Marker googleMarker = mMap.addMarker(markerOptions);
        googleMarker.setTag(marker);
    }


    @Override
    public void showDescription(String description) {
        textView.setVisibility(View.VISIBLE);
        textView.setText(description);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        presenter.userPressedOnMarker((MarkerModel) marker.getTag());
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        textView.setVisibility(View.GONE);
    }
}
