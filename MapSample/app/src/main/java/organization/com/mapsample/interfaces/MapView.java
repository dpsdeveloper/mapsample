package organization.com.mapsample.interfaces;

/**
 * Interface view, designed for encapsulate view layer.
 * <p>
 * Created by dps on 9/27/17.
 */
public interface MapView {
    /**
     * Might be called by presenter to show marker description.
     *
     * @param description - String value.
     */
    void showDescription(String description);
}
