package organization.com.mapsample.di.components;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import organization.com.mapsample.di.modules.ActivityDependentModule;
import organization.com.mapsample.ui.MapsActivity;

/**
 * Created by dps on 9/27/17.
 */
@Subcomponent(modules = ActivityDependentModule.class)
public interface MapsActivityComponent extends AndroidInjector<MapsActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MapsActivity> {
        public abstract Builder activityModule(ActivityDependentModule myActivityModule);

        @Override
        public void seedInstance(MapsActivity instance) {
            activityModule(new ActivityDependentModule(instance));
        }
    }

}
