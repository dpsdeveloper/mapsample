package organization.com.mapsample.presenter;

import javax.inject.Inject;

import io.reactivex.Observable;
import organization.com.mapsample.interfaces.MapPresenter;
import organization.com.mapsample.interfaces.MapView;
import organization.com.mapsample.models.bo.MarkerModel;
import organization.com.mapsample.models.dto.MarkerDTO;
import organization.com.mapsample.network.MarkersAPI;

/**
 * Class presenter, is in charge of manipulate view state.
 * <p>
 * Created by dps on 9/27/17.
 */
public class MapPresenterImpl implements MapPresenter {

    private MarkersAPI api;

    private MapView view;

    @Inject
    public MapPresenterImpl(MarkersAPI api, MapView view) {

        this.api = api;
        this.view = view;
    }

    @Override
    public void userPressedOnMarker(MarkerModel marker) {
        // TODO -> additional logic of processing description might be implemented
        view.showDescription(marker.description);
    }

    @Override
    public Observable<MarkerModel> getData() {
        return api.getMarkers()
                .flatMap(Observable::fromIterable)
                .map(this::convert)
                .cache();
    }

    public MarkerModel convert(MarkerDTO dto) {
        return new MarkerModel(dto.lat, dto.lng, dto.description);
    }
}
